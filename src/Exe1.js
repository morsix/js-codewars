function TestExe() {}

TestExe.prototype.superSize = function(num) {
    var array = [];
    while (num != 0) {
        array.push(num % 10);
        num = Math.floor(num / 10);
    }
    array.sort(function(a, b) { return b - a });
    num = 0;

    array.forEach(function(element) {
        num = num * 10 + element;
    }, this);

    return num;
};