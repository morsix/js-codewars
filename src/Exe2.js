function TestExe() {}

TestExe.prototype.opposite = function(number) {

    return parseFloat(('' + number).startsWith('-') ? ('' + number).replace('-', '') : '-' + number);
};